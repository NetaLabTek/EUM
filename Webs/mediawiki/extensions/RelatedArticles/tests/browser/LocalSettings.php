<?php

$wgRelatedArticlesLoggingSamplingRate = 1;
$wgRelatedArticlesShowInFooter = true;
$wgRelatedArticlesShowInSidebar = true;
$wgRelatedArticlesUseCirrusSearch = true;
$wgRelatedArticlesOnlyUseCirrusSearch = false;
